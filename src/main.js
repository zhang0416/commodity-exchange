import Vue from 'vue'
import App from './App.vue'
//三级联动组件,引入全局组件
import TypeNav from '@/components/typeNav'
import Carousel from '@/components/Carousel'
import Pagiantion from '@/components/Pagination'
//引入store仓库
import store from '@/store'
//引入路由
import router from '@/router'
//引入mock的数据
import '@/mock/mockServe'
//引入swiper轮播图
import "swiper/css/swiper.css";
//统一引入所有API
import * as API from '@/api';
//按需引入element-ui的组件
import {MessageBox} from 'element-ui';
//引入懒加载插件
import VueLazyload from "vue-lazyload";
//引入懒加载默认图片
import LF from '@/assets/1.gif';
//引入自定义插件
import myPlugins from '@/plugins/myPlugins';
//引入表单验证插件
import '@/plugins/validate';
//使用自定义插件
Vue.use(myPlugins);
// 注册全局组件
Vue.component(TypeNav.name,TypeNav)
Vue.component(Carousel.name,Carousel)
Vue.component(Pagiantion.name,Pagiantion)
//注册插件
Vue.use(VueLazyload,{
  //懒加载默认图片
  loading:LF
})
// 挂载到原型上
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.config.productionTip = false


new Vue({
  render: h => h(App),
  //配置全局事件总线$bus
  beforeCreate() {
    Vue.prototype.$bus=this;
    Vue.prototype.$API=API;
  },
  //注册路由
  router,
//  注册store仓库
  store
}).$mount('#app')
