import {v4 as uuidv4} from 'uuid';
//生成一个随机字符串，每次执行不能发生变化，使用本地存储
export const getUUID=()=>{
    let uuid_token=localStorage.getItem('UUID');
    if(!uuid_token){
        //生成临时身份
        uuid_token=uuidv4();
        //存储到本地
        localStorage.setItem('UUID',uuid_token)
    }
    return uuid_token;
}