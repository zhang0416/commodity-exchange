import {reqCategoryList, reqFloorList, reqGetBannerList} from "@/api";

//state:存储数据的地方
const state={
    //三级联动数据
    categoryList:[],
    //ListContainer组件的轮播图数据
    bannerList:[],
    // floor组件的轮播
    floorList:[]
};
//mutations:修改数据的唯一方法
const mutations = {
    CATEGORYLIST(state,categoryList){
        state.categoryList=categoryList
    },
    GETBANNERLIST(state,bannerList) {
        state.bannerList=bannerList
    },
    GETFLOORLIST(state,floorList) {
        state.floorList=floorList
    }
};
//actions:可以书写自己的业务逻辑，也可以处理异步
const actions = {
   // 获取三级联动数据
   async categoryList({commit}) {
    let result =await reqCategoryList();
       if(result.code===200) {
           commit('CATEGORYLIST',result.data)
       }
    },
    //获取轮播图
    async getBannerList({commit}) {
       let result= await reqGetBannerList();
        if(result.code===200) {
            commit('GETBANNERLIST',result.data)
        }

    },
//    获取floor轮播图的数据
  async getFloorList({commit}){
       let result =await reqFloorList();
       if(result.code===200) {
           commit('GETFLOORLIST',result.data)
       }
    }
};
//getters:可以理解为计算属性，用于简化state里的数据，让组件获取数据更加方便
const getters = {};

export default {
    state,
    mutations,
    actions,
    getters
}