import {reqCartList, reqDeleteCartById, reqUpdateCheckedById} from "@/api";

const state = {
    cartList: []
};
const mutations = {
    GETCARTLIST(state, cartList) {
        state.cartList = cartList
    },
};
const actions = {
//    获取购物车列表数据
    async getCartList({commit}) {
        let result = await reqCartList()
        if (result.code === 200) {
            commit('GETCARTLIST', result.data)
        }
    },
    // 删除购物车的数据
    async deleteCartListBySkuId({commit}, skuId) {
        let result = await reqDeleteCartById(skuId);
        if (result.code === 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('失败'));
        }
    },
//    修改购物车的选中状态
    async updateCheckedById({commit}, {skuId, isChecked}) {
        let result = await reqUpdateCheckedById(skuId, isChecked);
        if (result.code == 200) {
            return 'ok'
        } else {
            return Promise.reject(new Error('fail'))
        }
    },
    //  删除全部选中的商品
    deleteAllCheckedCart({dispatch, getters}) {
        let PromisrAll = [];
        getters.cartList.cartInfoList.forEach(item => {
            let promise = item.isChecked == 1 ? dispatch('deleteCartListBySkuId', item.skuId) : '';
            PromisrAll.push(promise);
        });
        //只有数组promiseAll里面的结果都成功才为成功，如果一个失败返回即为失败的结果
        return Promise.all(PromisrAll)
    },
//    修改全部产品的状态
    updateAllCartIsChecked({dispatch, state}, isChecked) {
        let promiseAll = [];
        state.cartList[0].cartInfoList.forEach(item => {
            let promise = dispatch("updateCheckedById", {skuId: item.skuId, isChecked});
            promiseAll.push(promise);
        })
        return Promise.all(promiseAll);
    }
};
const getters = {
    cartList(state) {
        return state.cartList[0] || {}
    },
};
export default {
    state,
    mutations,
    actions,
    getters
}
