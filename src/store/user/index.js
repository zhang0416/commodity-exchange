import {reqGetCode, reqUserRegister, reqUserLogin, reqUserInfo, reqLogOut} from "@/api";
import {setToken, getToken, removeToken} from "@/utils/token";

const state = {
    code: '',
    token: getToken(),
    userInfo: {},
};
const mutations = {
    GETCODE(state, code) {
        state.code = code
    },
    USERLOGIN(state, token) {
        state.token = token
    },
    GETUSERINFO(state, userInfo) {
        state.userInfo = userInfo
    },
    CLEAR(state) {
        //清空仓库的用户信息
        state.token = "",
            state.userInfo = {},
            // 清空本地存储的token
            removeToken();
    }
};
const actions = {
//    获取验证码
    async getCode({commit}, phone) {
        let result = await reqGetCode(phone);
        if (result.code === 200) {
            commit('GETCODE', result.data);
            return "ok"
        } else {
            return Promise.reject(new Error(result.message))
        }
    },
    // 注册
    async userRegister({commit}, user) {
        let result = await reqUserRegister(user);
        if (result.code == 200) {
            return "ok"
        } else {
            return Promise.reject(new Error(result.message))
        }
    },
    // 登录
    async userLogin({commit}, data) {
        let result = await reqUserLogin(data);
        if (result.code == 200) {
            commit("USERLOGIN", result.data.token);
            setToken(result.data.token);
            return "ok"
        } else {
            return Promise.reject(new Error(result.message))
        }
    },
    // 获取用户信息
    async getUserInfo({commit}) {
        let result = await reqUserInfo();
        if (result.code == 200) {
            commit('GETUSERINFO', result.data);
        };
        return result;
    },
//    退出登录
    async userLogOut({commit}) {
        let result = await reqLogOut();
        if (result.code == 200) {
            commit('CLEAR');
            return 'OK'
        } else {
            return Promise.reject(new Error(result.message))
        }
    }
};
const getters = {};

export default {
    state,
    mutations,
    actions,
    getters
}

