import {reqGetSearchInfo} from "@/api";
//state:存储数据的地方
const state = {
    searchList: {}
};
//mutations:修改数据的唯一方法
const mutations = {
    GETSEARCHLIST(state, searchList) {
        state.searchList = searchList
    }
};
//actions:可以书写自己的业务逻辑，也可以处理异步
const actions = {
    async getSearchList({commit}, params) {
        let res = await reqGetSearchInfo(params);
        if (res.code === 200) {
            commit('GETSEARCHLIST', res.data)
        }
    }
};
//getters:可以理解为计算属性，用于简化state里的数据，让组件获取数据更加方便
const getters = {
    //形参state为当前仓库的state
    goodsList(state) {
        return state.searchList.goodsList||[]
    },
    attrsList(state) {
        return state.searchList.attrsList||[]
    },
    trademarkList(state) {
        return state.searchList.trademarkList||[]
    }
};

export default {
    state,
    mutations,
    actions,
    getters
}