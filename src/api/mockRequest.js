//二次封装axios
import axios from "axios";
//引入进度条
import nprogress from 'nprogress'
//引入进度条样式
import 'nprogress/nprogress.css'
//start方法代表进度条开始，done方法代表进度条结束


//利用axios对象的create，创建一个axios实例
//request就是axios
const requests = axios.create({
    //配置对象
    //基础路径,发请求的时候路径中会出现api
    baseURL:'/mock',
    //设置请求超时时间
    timeout:5000,
});
//创建请求拦截器

requests.interceptors.request.use((config)=>{
//    config: 配置对象，对象里有个很重要的属性，header请求头
    nprogress.start();
    return config;
});

//创建响应拦截器
requests.interceptors.response.use((res)=>{
    nprogress.done();
  return res.data;
},(error)=>{
    return Promise.reject(new Error('faile'))
})

export default requests;