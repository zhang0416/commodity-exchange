// api接口进行统一管理
import requests from "@/api/request";
import mockRequests from "@/api/mockRequest";

//三级联动接口
// /api/product/getBaseCategoryList   GET    无参数
//第一种写法
/*export function reqCategoryList() {
  return requests({
        url:'/product/getBaseCategoryList',
        method:'GET',
    })
}*/
// 第二种写法
export const reqCategoryList = () => requests.get('/product/getBaseCategoryList');


//获取主页的轮播图
//第一种写法
/*export function reqGetBannerList() {
    return mockRequests({
        url:'/banner',
        method:'GET'
    })
}*/
// 第二种写法
export const reqGetBannerList = () => mockRequests.get('/banner')

//获取floor组件的数据
//第一种写法
/*export function reqFloorList() {
    return mockRequests({
        url:'/floor',
        method:'GET'
    })
}*/
// 第二种写法
export const reqFloorList = () => mockRequests.get('/floor')

//获取搜索模块数据   /api/list    post
//第一种写法
/*export function reqGetSearchInfo(params) {
    return requests({
        url:'/api/list',
        method:'POST',
        data:params
    })
}*/
//第二种写法
export const reqGetSearchInfo = (params) => requests({
    url: '/list',
    method: 'POST',
    data: params
})
//获取产品详情信息
//第一种写法
/*export function reqGoodsInfo(skuId) {
    return requests({
        url:`/item/${skuId}`,
        method:'GET',
    })
}*/
//第二种写法
export const reqGoodsInfo = (skuId) => requests({
    url: `/item/${skuId}`,
    method: 'GET'
})

//将产品添加到购物车或者更新产品个数
export const reqAddOrUpdateShopCart = (skuId, skuNum) => requests({
    url: `/cart/addToCart/${skuId}/${skuNum}`,
    method: 'POST'
})

//获取购物车的数据
export const reqCartList = () => requests({
    url: `/cart/cartList`,
    method: 'GET'
})

//删除购物车的商品
export const reqDeleteCartById = (skuId) => requests({
    url: `/cart/deleteCart/${skuId}`,
    method: 'DELETE'
})

//修改购物车商品选中状态
export const reqUpdateCheckedById = (skuId, isChecked) => requests({
    url: `/cart/checkCart/${skuId}/${isChecked}`,
    method: 'GET',
})
//获取验证码
export const reqGetCode = (phone) => requests({
    url: `/user/passport/sendCode/${phone}`,
    method: 'GET'
})

//注册
export const reqUserRegister = (data) => requests({
    url: '/user/passport/register',
    method: 'POST',
    data,
})

//登录
export const reqUserLogin = (data) => requests({
    url: '/user/passport/login',
    method: 'POST',
    data,
})
//获取用户信息
export const reqUserInfo = () => requests({
    url: '/user/passport/auth/getUserInfo',
    method: 'GET'
})

//退出登录
export const reqLogOut = () => requests({
    url: '/user/passport/logout',
    method: 'GET',
})
//获取用户地址信息
export const reqAddressInfo = () => requests({
    url: '/user/userAddress/auth/findUserAddressList',
    method: 'GET',
})
//获取订单交易页信息
export const reqOrderInfo = () => requests({
    url: '/order/auth/trade',
    method: 'GET',
})
//提交订单
export const reqSubmitrder = (tradeNo, data) => requests({
    url: `/order/auth/submitOrder?tradeNo=${tradeNo}`,
    method: 'POST',
    data,
})
//获取用户订单支付信息
///api/payment/weixin/createNative/{orderId}
export const reqPayInfo = (orderId) => requests({
    url: `/payment/weixin/createNative/${orderId}`,
    method: 'GET',
})
//获取支付是否成功
export const reqPayStatus=(orderId)=>requests({
    url:`/payment/weixin/queryPayStatus/${orderId}`,
    method:'GET',
})
//获取个人中心数据
export const reqMyOrderList=(page,limit)=>requests({
    url:`/order/auth/${page}/${limit}`,
    method:'GET',
})
