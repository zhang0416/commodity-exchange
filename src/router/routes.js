export default [
    //主页
    {
        path: '/home',
        component: () => import('@/pages/Home'),
        meta: {
            show: true
        }
    },
    //搜索
    {
        path: '/search/:keyword?',
        name: 'search',
        component: () => import('@/pages/Search'),
        meta: {
            show: true
        }
    },
    //商品详情页
    {
        path: '/detail/:skuid',
        name: 'detail',
        component: () => import('@/pages/Detail'),
        meta: {
            show: true
        }
    },
    //登录
    {
        path: '/login',
        component: () => import('@/pages/Login'),
        meta: {
            show: false
        }
    },
    //注册
    {
        path: '/register',
        component: () => import('@/pages/Register'),
        meta: {
            show: false
        }
    },
    //添加购物车成功页
    {
        path: '/AddCartSuccess',
        name: 'AddCartSuccess',
        component: () => import('@/pages/AddCartSuccess'),
        meta: {
            show: true
        }
    },
    //购物车
    {
        path: '/ShopCart',
        name: 'ShopCart',
        component: () => import('@/pages/ShopCart'),
        meta: {
            show: true
        }
    },
    //结算
    {
        path: '/trade',
        component: () => import('@/pages/Trade'),
        meta: {
            show: true
        },
        //路由独享守卫
        beforeEnter: (to, from, next) => {
            if (from.path == '/shopCart') {
                console.log(from);
                next();
            } else {
                next(false);
            }
        }
    },
    //支付
    {
        path: '/pay',
        component: () => import('@/pages/Pay'),
        meta: {
            show: true
        },
        //路由独享守卫
        beforeEnter: (to, from, next) => {
            if (from.path == '/trade') {
                next();
            } else {
                next(false);
            }
        }
    },
    //支付成功页面
    {
        path: '/paysuccess',
        component: () => import('@/pages/PaySuccess'),
        meta: {
            show: true
        }
    },
    //个人中心页面
    {
        path: '/center',
        component: () => import('@/pages/Center/myOrder'),
        meta: {
            show: true
        },
        // 二级路由
        children: [
            {
                path: 'myOrder',
                component: () => import('@/pages/Center/myOrder')
            },
            {
                path: 'groupOrder',
                component: () => import('@/pages/Center/groupOrder')
            },
            {
                path: '/center',
                redirect: '/center/myOrder'
            }
        ]
    },
    //    重定向
    {
        path: '*',
        redirect: '/home'
    }
]
