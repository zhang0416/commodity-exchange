//引入vue和vuerouter
import Vue from 'vue';
import VueRouter from "vue-router";
// 使用vuerouter
Vue.use(VueRouter);
import routes from "./routes";
//引入store
import store from '@/store';
//重写push/replace
let originPush = VueRouter.prototype.push;
let originReplace = VueRouter.prototype.replace;
//第一个参数是告诉push方法你往哪里跳转，（传递那些参数）
VueRouter.prototype.push = function (location, resolve, reject) {
    if (resolve && reject) {
        originPush.call(this, location, resolve, reject);
    } else {
        originPush.call(this, location, () => {
        }, () => {
        })
    }
}
VueRouter.prototype.replace = function (location, resolve, reject) {
    if (resolve && reject) {
        originReplace.call(this, location, resolve, reject);
    } else {
        originReplace.call(this, location, () => {
        }, () => {
        });
    }
}
// 配置路由
let router = new VueRouter({
//    配置路由
    routes,
    scrollBehavior(to, from, savedPosition) {
        return {y: 0}
    }
})
router.beforeEach(async (to, from, next) => {
    let token = store.state.user.token;
    let name = store.state.user.userInfo.name;
    console.log(store.state.user)
    if (token) {
        //如果登录后，禁止跳转到login页面
        if (to.path == '/login' || to.path == '/register') {
            next('/');
        } else {
            if (name) {
                next();
            } else {
                store.dispatch('getUserInfo');
                next();
                let result=await store.dispatch("getUserInfo");
                console.log(result);
                if(result.code==200){
                    next();
                }else {
                    store.dispatch("userLogOut");
                    next('/login');
                }
                /*                try {
                                    let result = await store.dispatch('getUserInfo');
                                    console.log(result);
                                    next();
                                } catch (e) {
                                    // token失效了
                                    await store.dispatch("userLogOut");
                                    next('/login')
                                }*/
            }
        }
    } else {
        //未登录
        let toPath=to.path;
        if(toPath.indexOf('/trade')!=-1||toPath.indexOf('/pay')!=-1||toPath.indexOf('/center')!=-1){
            next('/login?redirect='+toPath)
        }else {
            next();
        }
    }
})
export default router;
