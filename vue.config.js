module.exports = {
    //关闭语法检查
    lintOnSave: false,
    //不打包map文件
    productionSourceMap:false,
    //配置跨域代理proxy
    devServer: {
        proxy: {
            '/api': {
                target: 'http://39.98.123.211',
                changeOrigin: true,
            },
        }
    }
}
